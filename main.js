// stuff
function fetchApi(endpoint, arg) {
    return fetch(endpoint + "/" + arg)
        .then(response => response.json())
        .catch(error => console.error('Error:', error));
}

// even more stuff
function mapResult(result){
    return result.map((elem) => {
        // Do something with elem
        // Example: returning the element itself
        return elem;
    });
}

// stuffissima
async function main(){
    try {
        let result = await fetchApi("http://mia.prova/v1", "topolino");
        let mappedResult = mapResult(result);
        console.log(mappedResult);
    } catch (error) {
        console.error('Error in main function:', error);
    }
}

main();